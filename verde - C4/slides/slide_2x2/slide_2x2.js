app.register("slide_2x2", function () {
    return {
        events: {
            'swiperight #slide_2x2': 'noSwipe',
            'swipeleft #slide_2x2': 'noSwipe',
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {
        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            productFamily = "Berocca";
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
           
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            
            var filtro1 = el.target.dataset.filtro1;
            var filtro2 = el.target.dataset.filtro2;
            
            if( filtro1!==undefined ){
                busqueda_filtro1 = filtro1;
            }
            if( filtro2!==undefined ){
                busqueda_filtro2 = filtro2;
            }            
            
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            setTimeout(function(){
                app.goTo(go);
            }, 300);
            
        }
    };
});


