app.register("slide_1x7", function () {

    return {
        events: {
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            "tap #popup1_3_1_btn_video": 'videoPop',
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-supra': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        videoPop: function(ele){
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    }

});