app.register("slide_1x6", function () {

    return {
        events: {
            "tap .js-open-popup": 'openPopup',
            "tap .js-close-popup": 'closePopup',
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-supra': 'videoPop'
        },
        states: [],
        onRender: function (el) {            

//            $("#master").slider({
//                value: 0,
//                orientation: "horizontal",
//                range: "min",
//                animate: true,
//                slide: function (e, ui) {
//                    var val = ui.value;
//                    console.log(val);
//                    if (val >= 0 && val <= 25) {
//                        $(".cara-1").addClass("show");
//                        $(".cara-2, .cara-3, .cara-4").removeClass("show");
//                    } else if (val >= 26 && val <= 50) {
//                        $(".cara-2").addClass("show");
//                        $(".cara-1, .cara-3, .cara-4").removeClass("show");
//                    } else if (val >= 51 && val <= 75) {
//                        $(".cara-3").addClass("show");
//                        $(".cara-1, .cara-2, .cara-4").removeClass("show");
//                    } else if (val >= 76 && val <= 100) {
//                        $(".cara-4").addClass("show");
//                        $(".cara-1, .cara-2, .cara-3").removeClass("show");
//                    }
//                }
//            });

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Supradyn";
            document.removeEventListener('swipeleft', app.slideshow.right);
            document.removeEventListener('swiperight', app.slideshow.left);
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);

//            updateSliderValue(window.slider_cabeza.position[0], 0);

        },
        onExit: function (el) {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        openPopup: function (ele) {
            var pop = ele.target.dataset.popup;
            $(pop).addClass('active');
            $(".overlay").fadeIn(200);
            $(".logo").fadeOut(200);
        },
        closePopup: function () {
            $('.popup').removeClass('active');
            $(".overlay").fadeOut(200);
            $(".logo").fadeIn(200);
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
        }
    };

});


//function updateSliderValue(x, y) {
//
//    console.log(value);
//
//
//    if (value >= 0 && value <= 30) {
//        $(".cara-1").fadeIn(200);
//        $(".cara-2, .cara-3, .cara-4").fadeOut(10);
//    } else if (value >= 31 && value <= 58) {
//        $(".cara-2").fadeIn(200);
//        $(".cara-1, .cara-3, .cara-4").fadeOut(10);
//    } else if (value >= 59 && value <= 88) {
//        $(".cara-3").fadeIn(200);
//        $(".cara-1, .cara-2, .cara-4").fadeOut(10);
//    } else if (value >= 89 && value <= 100) {
//        $(".cara-4").fadeIn(200);
//        $(".cara-1, .cara-2, .cara-3").fadeOut(10);
//    }
//}