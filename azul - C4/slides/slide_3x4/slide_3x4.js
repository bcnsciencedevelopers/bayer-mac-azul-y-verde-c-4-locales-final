app.register("slide_3x4", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipeup: pararEventos,
            "tap .js-ver-video-bep": 'videoPop',
            'tap .js-select-product': 'selectProduct',
            "tap #anuciotv_3x4": 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Bepanthol";

        },
        onExit: function (el) {
            

            cerrar_popup(3, 4);

            $("#slide_3x4llamada_popup1, #slide_3x4llamada_popup2").addClass("oculto");

            $("#box3x4x1").removeClass("caja3x4x1_long");
            $("#box3x4x2").removeClass("caja3x4x2_long");
            $("#box3x4x1").removeClass("cortador3x4x1");
            $("#box3x4x2").removeClass("cortador3x4x2");

            $("#title3x4").removeClass();
            $("#reference3x4").removeClass();

            $("#title3x4").addClass("titulo3x4x0");
            $("#reference3x4").addClass("referencia3x4x0");
        },
        videoPop: function(ele){
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    };
});
function click_in_3x4(num) {

    $("#title3x4").removeClass();
    $("#reference3x4").removeClass();

    if ($("#box3x4x" + num).hasClass("caja3x4x" + num + "_long")) {

        $("#title3x4").addClass("titulo3x4x0");
        $("#reference3x4").addClass("referencia3x4x0");

        $("#box3x4x" + num).removeClass("caja3x4x" + num + "_long");
        $("#slide_3x4llamada_popup" + num).addClass("oculto");

        if (num == 1) {
            $("#box3x4x2").removeClass("cortador3x4x2");
        } else {
            $("#box3x4x1").removeClass("cortador3x4x1");
        }

    } else {

        $("#title3x4").addClass("titulo3x4x" + num);
        $("#reference3x4").addClass("referencia3x4x" + num);

        $("#box3x4x" + num).removeClass("cortador3x4x" + num);
        $("#box3x4x" + num).addClass("caja3x4x" + num + "_long");
        $("#slide_3x4llamada_popup" + num).removeClass("oculto");

        if (num == 1) {
            $("#box3x4x2").removeClass("caja3x4x2_long");
            $("#box3x4x2").addClass("cortador3x4x2");
            $("#slide_3x4llamada_popup2").addClass("oculto");
        } else {
            $("#box3x4x1").removeClass("caja3x4x1_long");
            $("#box3x4x1").addClass("cortador3x4x1");
            $("#slide_3x4llamada_popup1").addClass("oculto");
        }

    }
}