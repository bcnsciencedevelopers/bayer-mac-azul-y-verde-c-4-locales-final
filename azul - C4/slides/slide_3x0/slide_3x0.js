app.register("slide_3x0", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipeup: pararEventos,
            swipedown: pararEventos,
            'tap #card1': flip1,
            'tap #card2': flip2,
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-bep': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {
        },
        onEnter: function (el) {
            productFamily = "Bepanthol";
        },
        onExit: function (el) {
            $("#card1 .front1, #card2 .front2").show();
            $("#card1 .back1, #card2 .back2").hide();
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        }
    };

});
function flip1() {
    $("#card1 .front1, #card1 .back1").toggle("fade");

}
function flip2() {
    $("#card2 .front2, #card2 .back2").toggle("fade");

}
function flip3x0(num) {
    if ($("#icon3x0x" + num + "x2").hasClass("resaltar3x0")) {
        $("#icon3x0x" + num + "x2").removeClass("resaltar3x0");
    } else {
        $("#icon3x0x" + num + "x2").addClass("resaltar3x0");
    }
}

function changeIMG3x0(num) {
    $('#f' + num + '_card').toggleClass('animacionTransform');
}

