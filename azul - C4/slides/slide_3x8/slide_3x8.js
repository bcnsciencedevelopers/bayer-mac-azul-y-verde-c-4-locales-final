app.register("slide_3x8", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipedown: pararEventos,
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-bep': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Bepanthol";

        },
        onExit: function (el) {
            
            cerrar_popup(3, 8);
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        }


    };

});
