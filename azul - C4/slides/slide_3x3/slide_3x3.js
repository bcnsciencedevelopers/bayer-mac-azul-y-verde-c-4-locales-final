app.register("slide_3x3", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-bep': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {

            productFamily = "Bepanthol";
            $('.slide3x3').on('click', function () {
                var id = $(this).attr('id').split('3x').pop();
                for (var i = 1; i <= 4; i++) {
                    if (i == id) {
                        $('#slide_3x3_bg_' + id).fadeIn("slow");
                    } else {
                        $('#slide_3x3_bg_' + i).fadeOut("slow");
                    }
                }

            });
        },
        onExit: function (el) {

            $('#slide_3x3_bg_1').fadeIn("slow");
            $('#slide_3x3_bg_2').fadeOut("slow");
            $('#slide_3x3_bg_3').fadeOut("slow");
            $('#slide_3x3_bg_4').fadeOut("slow");
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        }
    };

});