app.register("slide_4x2", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-iber': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Iberogast";
            $('.radio20').on('click', function () {
                $('.img20, .radio20').fadeOut("slow");
                $('.img50, .radio50').removeClass('hidden').fadeIn("slow");
            });

            $('.radio50').on('click', function () {
                $('.img50, .radio50').fadeOut("slow");
                $('.img20, .radio20').removeClass('hidden').fadeIn("slow");

            });

        },
        onExit: function (el) {
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        }
    };

});
