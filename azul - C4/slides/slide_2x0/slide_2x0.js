app.register("slide_2x0", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Canespie-Funsol";
        },
        onExit: function (el) {
        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    };

});