var comprobar = 0;

app.register("slide_1x0", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            'tap .js-select-product': 'selectProduct'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Gine-Canesten";
        },
        onExit: function (el) {
            cerrar_popup(1, 0);
        },
        selectProduct: function (el) {
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            var file = ele.target.dataset.file;
            console.log(file);
            $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                .on("swipedown swipeup swiperight swipeleft", function (e) {
                    e.stopPropagation();
                })
                .on("click", function (event) {
                    if ($(event.target).is(":not(video)")){
                        event.stopPropagation();
                        event.preventDefault();
                        $(this).remove();
                    }
                }).appendTo("#presentation");
            var v = $("#presentation").find('.videoPopup video').get(0);
            if (v.load) v.load();
        }
    };
});