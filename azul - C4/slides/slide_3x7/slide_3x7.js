app.register("slide_3x7", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipeup: pararEventos,
            "tap .js-ver-video-bep": 'videoPop',
            'tap .js-select-product': 'selectProduct',
            "tap #anuciotv_3x4": 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Bepanthol";

        },
        onExit: function (el) {

            $(".sello-pantenol").show();
            cerrar_popup(3, 7);

            $("#slide_3x7llamada_popup1, #slide_3x7llamada_popup2").addClass("oculto");

            $("#box3x7x1").removeClass("caja3x7x1_long");
            $("#box3x7x2").removeClass("caja3x7x2_long");
            $("#box3x7x1").removeClass("cortador3x7x1");
            $("#box3x7x2").removeClass("cortador3x7x2");

        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        },
        selectProduct: function (el) {
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        }
    };
});
function click_in_3x7(num) {


    if ($("#box3x7x" + num).hasClass("caja3x7x" + num + "_long")) {

        $(".sello-pantenol").fadeIn();
        
        $("#box3x7x" + num).removeClass("caja3x7x" + num + "_long");
        $("#slide_3x7llamada_popup" + num).addClass("oculto");

        if (num == 1) {
            $("#box3x7x2").removeClass("cortador3x7x2");
        } else {
            $("#box3x7x1").removeClass("cortador3x7x1");
        }

    } else {
        
        $(".sello-pantenol").fadeOut();
        
        $("#box3x7x" + num).removeClass("cortador3x7x" + num);
        $("#box3x7x" + num).addClass("caja3x7x" + num + "_long");
        $("#slide_3x7llamada_popup" + num).removeClass("oculto");

        if (num == 1) {
            $("#box3x7x2").removeClass("caja3x7x2_long");
            $("#box3x7x2").addClass("cortador3x7x2");
            $("#slide_3x7llamada_popup2").addClass("oculto");
        } else {
            $("#box3x7x1").removeClass("caja3x7x1_long");
            $("#box3x7x1").addClass("cortador3x7x1");
            $("#slide_3x7llamada_popup1").addClass("oculto");
        }

    }
    
}