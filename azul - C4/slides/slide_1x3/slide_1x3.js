var currentBg = 0;
var bgImages;
var bgAmount;

app.register("slide_1x3", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipeup: pararEventos,
            swipedown: pararEventos,
            'tap .js-select-product': 'selectProduct',
            'tap .js-nav': 'changeBackground',
            'tap .js-ver-video-gine': 'videoPop'
        },
        states: [],
        onRender: function (el) {
        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Gine-Canesten";


        },
        onExit: function (el) {
            $('#slide_1x3_bg').removeClass('content2');
            $('#slide_1x3_bg').removeClass('content3');
            a_content = 1;
            $('#slide_1x3_bg').addClass('content' + a_content);
            $('#slide_1x3_bg').data('content', a_content);
            console.log(a_content);
            cerrar_popup(1, 3);
        },
        videoPop: function (ele) {         

            var file = ele.target.dataset.file;
            setTimeout(function(){
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")) {
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load)
                    v.load();
            },400);
            
        },
        selectProduct: function (el) {
            alert('sdfa');
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        changeBackground: function (el) {
            var move = el.target.dataset.move;
            var a_content = parseInt($('#slide_1x3_bg').data('content'));

            if (move == 'next') {
                $('#slide_1x3_bg').removeClass('content' + a_content);
                a_content = a_content + 1;
                $('#slide_1x3_bg').addClass('content' + a_content);
                $('#slide_1x3_bg').data('content', a_content);
            }  else if (move == 'next+1') {
                $('#slide_1x3_bg').removeClass('content' + a_content);
                a_content = a_content + 2;
                $('#slide_1x3_bg').addClass('content' + a_content);
                $('#slide_1x3_bg').data('content', a_content);
            }
            
            
            else if (move == 'prev') {
                $('#slide_1x3_bg').removeClass('content' + a_content);
                a_content = a_content - 1;
                $('#slide_1x3_bg').addClass('content' + a_content);
                $('#slide_1x3_bg').data('content', a_content);
            }
            
            else if (move == 'prev-1') {
                $('#slide_1x3_bg').removeClass('content' + a_content);
                a_content = a_content - 2;
                $('#slide_1x3_bg').addClass('content' + a_content);
                $('#slide_1x3_bg').data('content', a_content);
            }
        }
    };

});
