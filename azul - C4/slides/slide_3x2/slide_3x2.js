app.register("slide_3x2", function () {

    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
            swipedown: pararEventos,
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-bep': 'videoPop',
            'tap .bgpopup-prev': 'prevPopup',
            'tap .bgpopup-next': 'nextPopup',
            'tap .reset_popup': 'prevPopup'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Bepanthol";


        },
        onExit: function (el) {
            cerrar_popup(3, 2);

        },
        selectProduct: function(el){
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function(ele){
            
            setTimeout(function(){
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                    .on("swipedown swipeup swiperight swipeleft", function (e) {
                        e.stopPropagation();
                    })
                    .on("click", function (event) {
                        if ($(event.target).is(":not(video)")){
                            event.stopPropagation();
                            event.preventDefault();
                            $(this).remove();
                        }
                    }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load) v.load();
            },500);
            
        },
        prevPopup: function(el){
            $('#slide_3x2popup1, #slide_3x2popup2').removeClass('ver2');
            $('.btn-producto').show();
        },
        nextPopup: function(el){
            $('#slide_3x2popup1, #slide_3x2popup2').addClass('ver2');
            $('.btn-producto').hide();
        }
    }

});