app.register("slide_4x1", function () {
    var animations = [];
    return {
        events: {
            swipeleft: pararEventos,
            swiperight: pararEventos,
//            'tap #slide_4x1llamada_popup3': 'animationPopUp',
            'tap .js-select-product': 'selectProduct',
            'tap .js-ver-video-iber': 'videoPop'
        },
        states: [],
        onRender: function (el) {

        },
        onRemove: function (el) {

        },
        onEnter: function (el) {
            productFamily = "Iberogast";
        },
        onExit: function (el) {
            cerrar_popup(4, 1);
//            $('.paso-1').css('opacity', 1);
//            $('.paso-2, .paso-3, .paso-4, .paso-5').css('opacity', 0);

            animations.forEach(function (item) {
                clearTimeout(item);
            });
        },
        noSwipe: function (e) {
            e.stopPropagation();
        },
        selectProduct: function (el) {
            var go = el.target.dataset.goto;
            var filtro1 = el.target.dataset.filtro1;
            busqueda_filtro1 = filtro1;
            getMaterialFromXML(material_select);
            getAllListFromXML(material);
            app.goTo(go);
        },
        videoPop: function (ele) {

            setTimeout(function () {
                var file = ele.target.dataset.file;
                console.log(file);
                $("<div class='videoPopup'><video src='" + file + "' controls autoplay/><div id='js-close-videos'></div></div>")
                        .on("swipedown swipeup swiperight swipeleft", function (e) {
                            e.stopPropagation();
                        })
                        .on("click", function (event) {
                            if ($(event.target).is(":not(video)")) {
                                event.stopPropagation();
                                event.preventDefault();
                                $(this).remove();
                            }
                        }).appendTo("#presentation");
                var v = $("#presentation").find('.videoPopup video').get(0);
                if (v.load)
                    v.load();
            }, 500);

        }
//        animationPopUp: function (ele) {
//
//            animations[0] = setTimeout(function () {
//                $('.paso-1').animate({opacity: 0}, 1000);
//            }, 1500);
//            animations[1] = setTimeout(function () {
//                $('.paso-2').animate({opacity: 1}, 700);
//                $('.paso-1').animate({opacity: 0}, 2000);
//            }, 2000);
//            animations[2] = setTimeout(function () {
//                $('.paso-3').animate({opacity: 1}, 700);
//                $('.paso-2').animate({opacity: 0}, 1500);
//            }, 4000);
//            animations[2] = setTimeout(function () {
//                $('.paso-4').animate({opacity: 1}, 700);
//                $('.paso-3').animate({opacity: 0}, 1500);
//            }, 6000);
//            animations[3] = setTimeout(function () {
//                $('.paso-5').animate({opacity: 1}, 700);
//                $('.paso-4').animate({opacity: 0}, 1500);
//            }, 8000);
//        }
    };
});

