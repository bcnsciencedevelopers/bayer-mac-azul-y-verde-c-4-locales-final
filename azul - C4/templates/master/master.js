(function () {
    'use strict';

    document.addEventListener('ready', function () {

        // Prevent vertical bouncing of slides if tablet or bigger
        document.ontouchmove = function (event) {
            var currentWidth = app.dom.get('wrapper').getBoundingClientRect().width;
            if (currentWidth >= 768)
                event.preventDefault();
        };

        if (window.ag && window.ag.data) {
            ag.data.getPresenter(); // data available through ag.data.presenter
            ag.data.getCallContacts(); // data available through ag.data.call_contacts
        }

    });

});

//FUNCIONES PROPIAS DEL MATERIAL
function abrir_popup(group, slide, num) {
    $("#slide_" + group + "x" + slide + "capa_popup").removeClass("oculto");
    $("#slide_" + group + "x" + slide + "popup" + num).removeClass("oculto");
    $(".logo").addClass("oculto");
    $(".boton_expositor").addClass("oculto");
    $(".btn-tv").addClass("oculto");
//    alert("#slide_" + group + "x" + slide + "capa_popup" + num);
};

function cerrar_popup(group, slide) {
    $(".logo").removeClass("oculto");
    $(".boton_expositor").removeClass("oculto");
    $(".btn-tv").removeClass("oculto");
    $("#slide_" + group + "x" + slide + "capa_popup").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup1").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup2").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup3").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup4").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup5").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup6").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup7").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup8").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup9").addClass("oculto");
    $("#slide_" + group + "x" + slide + "popup10").addClass("oculto");
};


function abrir_popup_especial3x() {
    $("#especial3xcapa_popup").removeClass("oculto");
    $("#especial3xpopup").removeClass("oculto");
    $(".logo").addClass("oculto");
}

function cerrar_popup_especial3x() {
    $(".logo").removeClass("oculto");
    $("#especial3xcapa_popup").addClass("oculto");
    $("#especial3xpopup").addClass("oculto");
}
function abrir_popup_especial3x2() {
    $("#especial3x2capa_popup").removeClass("oculto");
    $("#especial3x2popup").removeClass("oculto");
    $(".logo").addClass("oculto");
}

function cerrar_popup_especial3x2() {
    $(".logo").removeClass("oculto");
    $("#especial3x2capa_popup").addClass("oculto");
    $("#especial3x2popup").addClass("oculto");
}


function pararEventos(e){
    e.stopPropagation();
    e.preventDefault();
}

var renderProducto = {
    render: '',
    carpeta: '',
    color: '',
    id: '',
    nombre: '',
    descripcion: '',
    img:'',
    codigo: '',
    medidas: '',
    tipo: '',
    producto: '',
    baldas: '',
    logo: '',
    informacion: ''
};



function initKeyShotVR() {
    var nameOfDiv = "render-producto";
    var folderName = "slides/producto/" + renderProducto.carpeta;
    var viewPortWidth = 1050;
    var viewPortHeight = 901;
    var backgroundColor = "transparent";
    var uCount = 30;
    var vCount = 1;
    var uWrap = true;
    var vWrap = false;
    var uMouseSensitivity = -0.0833333;
    var vMouseSensitivity = 1;
    var uStartIndex = 29;
    var vStartIndex = 0;
    var minZoom = 1;
    var maxZoom = 1;
    var rotationDamping = 0.96;
    var downScaleToBrowser = true;
    var addDownScaleGUIButton = false;
    var downloadOnInteraction = true;
    var imageExtension = "png";
    var showLoading = false;
    var loadingIcon = "ks_logo.png"; // Set to empty string for default icon.
    var allowFullscreen = true; // Double-click in desktop browsers for fullscreen.
    var uReverse = true;
    var vReverse = false;
    var hotspots = {};
    $('#' + nameOfDiv).html('');
    renderProducto.render = new keyshotVR(nameOfDiv, folderName, viewPortWidth, viewPortHeight, backgroundColor, uCount, vCount, uWrap, vWrap, uMouseSensitivity, vMouseSensitivity, uStartIndex, vStartIndex, minZoom, maxZoom, rotationDamping, downScaleToBrowser, addDownScaleGUIButton, downloadOnInteraction, imageExtension, showLoading, loadingIcon, allowFullscreen, uReverse, vReverse, hotspots);
}


var productFamily;

function selectProduct(){
    busqueda_filtro1 = productFamily;
}
